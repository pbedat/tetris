# tetris

The game.

## Journal

### 2019-09-15

Dear Reader,

It is mid september and the christmas treats are slowly creeping back into the supermarkets.
While I was staring and probably drooling at a box of vegan chocolate coated gingerbread, it struck me.
It is about time to start with my christmas preparations!

Don't get me wrong: I'm not planing to connect my home automation system (which I don't have) to a theme park
range arsenal of christmas widgets (which my wife hides in a dark and secret place).
No "Alexa: commence festivity". No way!
Unlike my wife, I have grown up in a family, that considered Christmas, as a happening to get over with. As a child it was awesome -- more time to play with the newly
acquired toys. Nowadays - thanks to my wife and her very festive family - Christmas is not gotten over with very quickly.
So I decided to move my tradition into the pre christmas season and started to create toys of software, so that at least others may enjoy the silent time.

This year I also want to write about the process, which is always quite chaotic, stressful, creative and educational.
With my blog, I will try to reflect a little bit more about the challenges of game development.
And who knows?
Maybe you, dear reader, will find some entertainment in my struggle to bring something playable out of the door this year.
