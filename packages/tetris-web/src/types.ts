import { Shape } from "pbedat-tetris-core";

export interface Block {
  x: number;
  y: number;
  color: string;
}

export interface RenderedGame {
  blocks: Block[];
  width: number;
  height: number;
  score: number;
  nextShape: Shape;
  level: number;
}
