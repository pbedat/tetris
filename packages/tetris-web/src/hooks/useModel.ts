import {
  syncGameLoop,
  GameState,
  Actions,
  Rotations,
  getLevel
} from "pbedat-tetris-core";
import { useState, useEffect, useRef } from "react";
import { Block, RenderedGame } from "../types";
import { flatMap, identity, compact } from "lodash";

const NO_ACTION: Actions = { type: "NOOP" };

export default function useModel() {
  const [state, setState] = useState<RenderedGame>();
  const [action, setAction] = useState<Actions>(NO_ACTION);

  const gameRef = useRef(syncGameLoop());

  useEffect(() => {
    const game = gameRef.current;

    const interval: number = setInterval(() => {
      const nextGame = game.next(action as any).value;
      if (nextGame.gameOver) {
        if (navigator.vibrate) {
          navigator.vibrate([500, 500, 1000]);
        }
        alert("GAME OVER");
        return clearInterval(interval);
      }
      setAction(NO_ACTION);
      setState(render(nextGame));
    }, 75);

    return () => clearInterval(interval);
  }, [action]);

  return {
    game: state,
    dispatch(dispatchedAction: Actions) {
      if (action.type === "NOOP") {
        setAction(dispatchedAction);
      }
    }
  };
}

function render(game: GameState): RenderedGame {
  const { width, height, cells, currentShape, score, nextShape } = game;
  const blocks: Block[] = [
    ...compact(
      flatMap(cells, (row, y) =>
        row.map((occupied, x) => occupied && { x, y, color: "gray" })
      )
    ),
    ...currentShape.cells.map(([x, y]) => ({ x, y, color: "blue" }))
  ];

  return { blocks, width, height, score, nextShape, level: getLevel(game) };
}
