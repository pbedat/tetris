import { Actions, Rotations } from "pbedat-tetris-core";
import { useRef, useEffect } from "react";

export default function useInput(onAction: (action: Actions) => void) {
  useEffect(() => {
    document.body.addEventListener("keydown", handleKeyPress);

    function handleKeyPress(ev: KeyboardEvent) {
      const key = ev.key;

      switch (key) {
        case "ArrowLeft":
          onAction({ type: "MOVE", direction: [-1, 0] });
          break;
        case "ArrowRight":
          onAction({ type: "MOVE", direction: [1, 0] });
          break;
        case "ArrowDown":
          onAction({ type: "MOVE", direction: [0, 1] });
          break;
        case "a":
          onAction({ type: "ROTATE", rotation: Rotations.Counterclockwise });
          break;
        case "d":
          onAction({ type: "ROTATE", rotation: Rotations.Clockwise });
          break;
      }
    }

    return () => document.body.removeEventListener("keydown", handleKeyPress);
  });
}
