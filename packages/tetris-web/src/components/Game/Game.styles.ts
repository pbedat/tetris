import styled from "styled-components";

export default {
  Score: styled.div``,
  Stats: styled.div`
    display: flex;
    flex-direction: column;
    margin: 1rem;

    font-family: monospace;
    text-transform: uppercase;

    h3 {
      margin-bottom: 0;
    }

    p {
      margin-top: 0;
    }
  `,
  NextShape: styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
  `
};
