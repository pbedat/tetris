import React, { useEffect, useState, useRef } from "react";
import { debounce } from "lodash";

import styled from "./Game.styles";
import { Block, RenderedGame } from "../../types";

function useBlockSize(gameWidth: number, gameHeight: number) {
  const [size, setSize] = useState(0);
  const ref = useRef<HTMLDivElement>(null);

  function calcBlockSize(element: HTMLDivElement) {
    console.log(element.clientWidth, element.clientHeight);
    return Math.floor(
      Math.min(
        element.clientWidth / gameWidth,
        element.clientHeight / gameHeight
      )
    );
  }

  useEffect(() => {
    if (ref.current) {
      setSize(calcBlockSize(ref.current));
    }

    const handleResize = debounce(() => {
      if (ref.current) {
        setSize(calcBlockSize(ref.current));
      }
    });

    window.addEventListener("resize", handleResize);
    window.addEventListener("orientationchange", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      window.removeEventListener("orientationchange", handleResize);
    };
  }, [ref.current]);

  return { ref, size };
}

const Game: React.FC<RenderedGame> = props => {
  const { height, width, blocks, score, nextShape, level } = props;

  return (
    <div style={{ display: "flex", flexGrow: 1 }}>
      <BlockMatrix height={height} width={width} blocks={blocks} />
      <styled.Stats>
        <styled.Score>
          <h3>Score</h3>
          <p>{score}</p>
          <h3>Level</h3>
          <p>{level}</p>
        </styled.Score>
        <styled.NextShape>
          <h3>Next</h3>
          <BlockMatrix
            height={4}
            width={4}
            blocks={nextShape.cells.map(([x, y]) => ({ x, y, color: "blue" }))}
          ></BlockMatrix>
        </styled.NextShape>
      </styled.Stats>
    </div>
  );
};

interface BlockMatrixProps {
  width: number;
  height: number;
  blocks: Block[];
}

const BlockMatrix: React.FC<BlockMatrixProps> = props => {
  const { width, height, blocks } = props;
  const { ref, size: blockSize } = useBlockSize(width, height);

  return (
    <div ref={ref} style={{ flexGrow: 1 }}>
      <div
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          border: "2px black solid",
          width: blockSize * width,
          height: blockSize * height
        }}
      >
        {blocks.map(block => (
          <Block size={blockSize} {...block} key={`${block.x}/${block.y}`} />
        ))}
      </div>
    </div>
  );
};

const Block: React.FC<Block & { size: number }> = props => {
  const { x, y, color, size } = props;

  const offsetX = x * size;
  const offsetY = y * size;

  return (
    <div
      style={{
        position: "absolute",
        border: "1px solid gray",
        background: color,
        width: size,
        height: size,
        transform: `translate(${offsetX}px, ${offsetY}px)`
      }}
    ></div>
  );
};

export default Game;
