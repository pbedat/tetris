import React, { useEffect } from "react";
import { Actions, Rotations } from "pbedat-tetris-core";

interface Props {
  onAction: (action: Actions) => void;
}

interface Button {
  label: string;
  action: Actions;
}

const actions: Record<string, Actions> = {
  left: { type: "MOVE", direction: [-1, 0] },
  right: { type: "MOVE", direction: [1, 0] },
  down: { type: "MOVE", direction: [0, 1] },
  rotateLeft: { type: "ROTATE", rotation: Rotations.Counterclockwise },
  rotateRight: { type: "ROTATE", rotation: Rotations.Clockwise }
};

const Input: React.FC<Props> = props => {
  const { onAction } = props;

  useEffect(() => {
    document.body.addEventListener("keydown", handleKeyPress);

    function handleKeyPress(ev: KeyboardEvent) {
      console.log(ev.key);
      const key = ev.key;

      switch (key) {
        case "ArrowLeft":
          onAction(actions.left);
          break;
        case "ArrowRight":
          onAction(actions.right);
          break;
        case "ArrowDown":
          onAction(actions.down);
          break;
        case "a":
          onAction(actions.rotateLeft);
          break;
        case "d":
          onAction(actions.rotateRight);
          break;
      }
    }
  }, []);

  const buttons: Button[] = [
    {
      label: "◀",
      action: { type: "MOVE", direction: [-1, 0] }
    },
    {
      label: "▼",
      action: { type: "MOVE", direction: [0, 1] }
    },
    {
      label: "▶",
      action: { type: "MOVE", direction: [1, 0] }
    },
    { label: "A", action: { type: "ROTATE", rotation: Rotations.Clockwise } },
    {
      label: "D",
      action: { type: "ROTATE", rotation: Rotations.Counterclockwise }
    }
  ];
  return (
    <div>
      {buttons.map(({ label, action }) => (
        <button
          key={label}
          onClick={() => onAction(action)}
          style={{ fontSize: "1.5rem", margin: 4, userSelect: "none" }}
        >
          {label}
        </button>
      ))}
    </div>
  );
};

export default Input;
