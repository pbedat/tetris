import React, { useState } from "react";
import { debounce } from "lodash";
import { isTouchDevice } from "../../utils";
import styled from "./Button.styles";

interface Props
  extends Omit<
    React.HTMLAttributes<HTMLButtonElement>,
    "onMouseUp" | "onMouseDown"
  > {
  onTrigger(): void;
}

const Button: React.FC<Props> = props => {
  const { children, onTrigger, style } = props;

  const trigger = useTrigger(onTrigger);

  const events = isTouchDevice()
    ? {
        onTouchStart: trigger.onMouseDown,
        onTouchEnd: trigger.onMouseUp,
        onTouchCancel: trigger.onMouseUp
      }
    : trigger;

  return (
    <styled.Button {...events} style={style || {}}>
      {children}
    </styled.Button>
  );
};

function useTrigger(emit: () => void) {
  const [_, setPressed] = useState(false);
  const [timer, setTimer] = useState(-1);

  function onMouseDown(ev: any) {
    setPressed(pressed => {
      if (pressed) {
        return pressed;
      }
      emit();
      setTimer(setTimeout(startTriggerInterval, 200));
      return true;
    });
  }

  function startTriggerInterval() {
    setTimer(setInterval(emit, 100));
  }

  function onMouseUp() {
    setPressed(false);
    clearTimeout(timer);
    clearInterval(timer);
  }

  return {
    onMouseDown,
    onMouseUp
  };
}

export default Button;
