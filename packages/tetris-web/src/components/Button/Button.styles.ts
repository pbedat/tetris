import styled from "styled-components";

export default {
  Button: styled.div`
    display: block;
    padding: 1rem;

    font-family: Arial, Helvetica, sans-serif;
    font-size: 2rem;

    user-select: none;
    touch-action: none;
  `
};
