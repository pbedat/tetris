import styled from "styled-components";

export default {
  GameboyLayout: styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
  `,
  GameboyControls: styled.div`
    display: flex;
    justify-content: space-around;
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  `,
  SegaLayout: styled.div`
    flex-grow: 1;
    display: flex;
    justify-content: space-around;
    align-items: flex-end;
  `,
  SegaControls: styled.div`
    display: flex;
    margin: 3rem;
  `,
  SegaScreen: styled.div`
    display: flex;
    flex-grow: 1;
    height: 100vh;
  `
};
