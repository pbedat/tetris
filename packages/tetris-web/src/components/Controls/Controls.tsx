import React, { useState, useEffect, CSSProperties } from "react";
import useInput from "../../hooks/useInput";
import Button from "../Button/Button";
import { Actions, Rotations } from "pbedat-tetris-core";
import styled from "./Controls.styles";

const actions: Record<string, Actions> = {
  left: { type: "MOVE", direction: [-1, 0] },
  right: { type: "MOVE", direction: [1, 0] },
  down: { type: "MOVE", direction: [0, 1] },
  rotateLeft: { type: "ROTATE", rotation: Rotations.Counterclockwise },
  rotateRight: { type: "ROTATE", rotation: Rotations.Clockwise }
};

interface Props {
  onAction: (action: Actions) => void;
}

const Controls: React.FC<Props> = props => {
  const { children, onAction } = props;

  useInput(onAction);
  const { isGameboy } = useLayout();

  return isGameboy ? (
    <styled.GameboyLayout>
      {children}
      <styled.GameboyControls>
        <DirectionButtons onAction={onAction} />
        <RotateButtons onAction={onAction} />
      </styled.GameboyControls>
    </styled.GameboyLayout>
  ) : (
    <styled.SegaLayout>
      <styled.SegaControls>
        <DirectionButtons onAction={onAction} />
      </styled.SegaControls>
      <styled.SegaScreen>{children}</styled.SegaScreen>
      <styled.SegaControls>
        <RotateButtons onAction={onAction} />
      </styled.SegaControls>
    </styled.SegaLayout>
  );
};

const DirectionButtons: React.FC<{
  onAction(action: Actions): void;
}> = props => {
  const { onAction } = props;
  return (
    <div style={{ display: "flex" }}>
      <Button
        onTrigger={() => onAction(actions.left)}
        style={{ marginRight: ".5rem" }}
      >
        ◀
      </Button>
      <Button
        onTrigger={() => onAction(actions.down)}
        style={{
          marginRight: ".7rem",
          marginTop: "2.5rem",
          transform: "rotate(-90deg)"
        }}
      >
        ◀
      </Button>
      <Button onTrigger={() => onAction(actions.right)}>▶</Button>
    </div>
  );
};

const RotateButtons: React.FC<{
  onAction: (action: Actions) => void;
}> = props => {
  const { onAction } = props;
  return (
    <div style={{ display: "flex" }}>
      <Button
        onTrigger={() => onAction(actions.rotateLeft)}
        style={{
          marginRight: "1rem",
          lineHeight: "2rem",
          width: "2rem",
          borderRadius: "2rem",
          border: "1px solid black",
          textAlign: "center",
          height: "2rem",
          marginTop: "2rem"
        }}
      >
        A
      </Button>
      <Button
        onTrigger={() => onAction(actions.rotateRight)}
        style={{
          lineHeight: "2rem",
          width: "2rem",
          borderRadius: "2rem",
          border: "1px solid black",
          textAlign: "center",
          height: "2rem"
        }}
      >
        D
      </Button>
    </div>
  );
};

function useLayout() {
  const [layout, setLayout] = useState(getLayout());

  useEffect(() => {
    window.addEventListener("orientationchange", handleLayoutChange);
    window.addEventListener("resize", handleLayoutChange);

    function handleLayoutChange() {
      setLayout(getLayout());
    }

    () => {
      window.removeEventListener("orientationchange", handleLayoutChange);
      window.removeEventListener("resize", handleLayoutChange);
    };
  }, []);

  return { layout, isGameboy: layout === Layouts.Gameboy };
}

function getLayout() {
  return window.innerWidth < window.innerHeight
    ? Layouts.Gameboy
    : Layouts.Sega;
}

enum Layouts {
  Gameboy,
  Sega
}

export default Controls;
