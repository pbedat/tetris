import React, { useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import useModel from "./hooks/useModel";
import Game from "./components/Game/Game";
import Controls from "./components/Controls/Controls";

const App: React.FC = prop => {
  const { game, dispatch } = useModel();

  return game ? (
    <Controls onAction={dispatch}>
      <Game {...game} />
    </Controls>
  ) : (
    <span>Loading...</span>
  );
};

/*

      <button
        onClick={ev => {
          if (!ref.current) {
            return;
          }
          document.exitFullscreen();
          ref.current.requestFullscreen();
        }}
        style={{ marginRight: "2rem" }}
      >
        Fullscreen
      </button>
      */

ReactDOM.render(<App />, document.getElementById("app"));
