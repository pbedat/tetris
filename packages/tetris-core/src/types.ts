export interface Shape {
  cells: Point[];
  center: Point;
}

export type Point = [number, number];

export interface GameState {
  clearedLines: number;

  readonly width: number;
  readonly height: number;

  gameOver?: boolean;

  /**
   * number of milliseconds to be elapsed until gravity is applied
   */
  speed: number;

  elapsedTime: number;

  /**
   * cells[y][x] === true => there is a block
   *
   * 20 rows
   * 10 columns
   */
  cells: Array<Array<boolean>>;

  currentShape: Shape;

  nextShape: Shape;

  score: number;
}

export interface MoveAction {
  readonly type: "MOVE";
  readonly direction: [-1, 0] | [1, 0] | [0, 1];
}

interface RotateAction {
  readonly type: "ROTATE";
  readonly rotation: Rotations;
}

export enum Rotations {
  Clockwise = 1,
  Counterclockwise = -1
}

interface Noop {
  readonly type: "NOOP";
}

export type Actions = MoveAction | RotateAction | Noop;
