import { compact, negate } from "lodash";

import { GameState, Actions } from "../../types";
import { addVector } from "../utils/add-vector";
import { getLevelSpeed } from "./get-level-speed";
import willClip from "./will-clip";

export default function getMotion(
  game: GameState,
  action: Actions,
  delay: number
) {
  const { elapsedTime } = game;
  const directions: Array<[number, number]> = compact([
    action.type === "MOVE" && action.direction,
    (elapsedTime + delay) % getLevelSpeed(game, delay) === 0 && [0, 1]
  ]);

  const motion = directions
    .filter(negate(willClip(game)))
    .reduce(addVector, [0, 0])
    .map(d => Math.min(d, 1)) as [number, number];

  return motion;
}
