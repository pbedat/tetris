import { GameState } from "../../types";
import getLevel from "./get-level";

export function getLevelSpeed(game: GameState, framerate: number) {
  return (
    Math.round((game.speed * ((20 - getLevel(game)) / 20)) / framerate) *
    framerate
  );
}
