import { curry } from "lodash";

import translateShape from "../reducers/translate-shape";
import { GameState } from "../../types";
import withGame from "../utils/with-game";
import { ofAny } from "../../utils";

function willClip(game: GameState, vector: [number, number]) {
  if (vector[0] === 0) {
    return false;
  }

  const nextGame = withGame("currentShape", translateShape(vector))(game);

  const { currentShape } = nextGame;

  const isOutOfBounds = ([x, y]: [number, number]) =>
    x < 0 || x >= nextGame.width;
  const clipsCells = ([x, y]: [number, number]) => !!nextGame.cells[y][x];

  const hasClipping = currentShape.cells.some(ofAny(isOutOfBounds, clipsCells));

  return hasClipping;
}

export default curry(willClip);
