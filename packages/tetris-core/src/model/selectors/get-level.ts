import { GameState } from "../../types";

/**
 * This is the "fixed-goal" implementation of the level
 * @param game
 */
export default function getLevel(game: GameState) {
  return Math.min(20, Math.floor(game.clearedLines / 10));
}
