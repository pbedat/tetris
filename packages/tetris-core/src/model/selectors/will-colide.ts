import { GameState } from "../../types";

export default function willCollide(game: GameState, vector: [number, number]) {
  return game.currentShape.cells.some(
    ([x, y]) =>
      y + vector[1] >= game.cells.length || game.cells[y + vector[1]][x]
  );
}
