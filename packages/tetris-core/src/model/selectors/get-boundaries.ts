import { Shape } from "../../types";

export function getBoundaries(shape: Shape) {
  let boundaries: [number, number, number, number] = [0, 0, 0, 0];

  for (const [x, y] of shape.cells) {
    const [left, top, right, bottom] = boundaries;
    boundaries = [
      Math.min(left, x),
      Math.min(top, y),
      Math.max(right, x),
      Math.max(bottom, y)
    ];
  }

  return boundaries;
}
