import { Shape } from "../../types";

export function collides(shape: Shape, cells: boolean[][]) {
  return shape.cells.some(([x, y]) => !cells[y + 1] || cells[y + 1][x]);
}
