import { GameState } from "../../types";

export default function isGameOver(game: GameState) {
  const { currentShape, cells } = game;
  return currentShape.cells.some(([x, y]) => cells[y][x]);
}
