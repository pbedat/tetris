import { GameState } from "../../types";

export default function settleShape(game: GameState): GameState {
  const { currentShape, cells } = game;

  const nextCells = cells.map((row, cY) =>
    currentShape.cells.some(([_, y]) => cY === y)
      ? row.map(
          (_, cX) =>
            _ || !!currentShape.cells.find(([x, y]) => x === cX && y === cY)
        )
      : row
  );

  return { ...game, cells: nextCells };
}
