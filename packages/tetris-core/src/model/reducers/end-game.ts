import { GameState } from "../../types";

export default function endGame(game: GameState) {
  return { ...game, gameOver: true };
}
