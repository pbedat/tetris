import { identity } from "lodash";

import { GameState } from "../../types";
import withGame from "../utils/with-game";
import getLevel from "../selectors/get-level";

const LINE_MULTIPLIER = [0, 40, 100, 300, 1200];

export default function score(game: GameState) {
  const { cells } = game;
  const completedRows = cells.filter(row => row.every(identity));

  const score = LINE_MULTIPLIER[completedRows.length] * (getLevel(game) + 1);

  return withGame("score", prevScore => prevScore + score)(game);
}
