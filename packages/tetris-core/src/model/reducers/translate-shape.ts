import { curry } from "lodash";

import { Shape } from "../../types";

function translateShape(vector: [number, number], shape: Shape): Shape {
  const { cells, center, ...rest } = shape;
  const [tX, tY] = vector;
  return {
    ...rest,
    cells: cells.map(([x, y]) => [x + tX, y + tY]),
    center: [center[0] + tX, center[1] + tY]
  };
}

export default curry(translateShape);
