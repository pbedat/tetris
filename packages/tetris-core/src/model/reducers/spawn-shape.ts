import { sample, curry, random } from "lodash";

import { Shape, GameState } from "../../types";
import shapes from "../factories/shapes";
import { getBoundaries } from "../selectors/get-boundaries";
import rotateShape from "./rotate-shape";
import translateShape from "./translate-shape";

/**
 *    0 1 2 3 4 5 6 7
 *   +--------------->
 * 0 |# #
 * 1 |  # #
 * 2 |
 * 3 |
 * 4 |
 *   v
 * @param gameState
 * @param args
 */
function spawnNextShape(gameState: GameState): GameState {
  const { nextShape } = gameState;

  const [left, , right] = getBoundaries(nextShape);

  const shapeWidth = right - left;

  const motion: [number, number] = [
    left + random(0, gameState.width - shapeWidth - 1),
    0
  ];

  const translatedShape: Shape = translateShape(motion, nextShape);

  return {
    ...gameState,
    currentShape: translatedShape,
    nextShape: createNextShape()
  };
}

export function createNextShape() {
  const nextShape = sample(shapes)!;
  const rotatedShape = rotateShape(random(-3, 4), nextShape);
  const [left, top] = getBoundaries(rotatedShape);
  const reoriginationVector: [number, number] = [
    left < 0 ? Math.abs(left) : 0,
    top < 0 ? Math.abs(top) : 0
  ];
  return translateShape(reoriginationVector, rotatedShape);
}

export default curry(spawnNextShape);
