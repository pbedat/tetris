import { range, identity } from "lodash";

import { GameState } from "../../types";
import { value } from "../../utils";

export function tryClearLines(game: GameState): GameState {
  const numRows = game.cells.length;
  const clearedField = game.cells.filter(row => !row.every(identity));
  const clearedLines = numRows - clearedField.length;

  const nextCells = [
    ...range(clearedLines).map(() => range(game.width).map(value(false))),
    ...clearedField
  ];

  return {
    ...game,
    cells: nextCells,
    clearedLines: game.clearedLines + clearedLines
  };
}
