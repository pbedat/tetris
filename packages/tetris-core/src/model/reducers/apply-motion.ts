import { flow } from "lodash";

import { MoveAction } from "../../types";
import { applyTime } from "./apply-time";
import { withoutClipping } from "../middleware/without-clipping";
import translateShape from "./translate-shape";
import withGame from "../utils/with-game";

export default function applyMotion(action: MoveAction, delay: number) {
  return flow(
    applyTime(delay),
    withoutClipping(withGame("currentShape", translateShape(action.direction)))
  );
}
