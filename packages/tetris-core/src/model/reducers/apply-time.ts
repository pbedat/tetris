import { GameState } from "../../types";

export function applyTime(delay: number) {
  return (game: GameState): GameState => ({
    ...game,
    elapsedTime: game.elapsedTime + delay
  });
}
