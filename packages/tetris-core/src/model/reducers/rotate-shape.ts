import { curry } from "lodash";

import { Shape } from "../../types";

/**
 * Rotates the the shape times * 90°
 * @param times The number of *times* the shape should be rotated by 90°.
 *  Negative values rotate the shape counter clockwise.
 *  Positive values clockwise.
 * @param shape
 */
function rotateShape(times: number, shape: Shape): Shape {
  if (times % 4 === 0) {
    return shape;
  }

  const angle = times * 1.570796;
  const [ox, oy] = shape.center;

  const rotatedShape: Shape = {
    ...shape,
    cells: shape.cells.map(([px, py]) => [
      Math.round(
        ox + Math.cos(angle) * (px - ox) - Math.sin(angle) * (py - oy)
      ),
      Math.round(oy + Math.sin(angle) * (px - ox) + Math.cos(angle) * (py - oy))
    ])
  };

  return rotatedShape;
}

export default curry(rotateShape);
