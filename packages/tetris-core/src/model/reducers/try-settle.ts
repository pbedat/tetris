import { GameState } from "../../types";
import { collides } from "../selectors/collides";
import { flow } from "lodash";
import { tryClearLines } from "./try-clear-lines";
import spawnNextShape from "./spawn-shape";
import settleShape from "./settle-shape";

export default function trySettle(
  game: GameState,
  otherwise: (game: GameState) => GameState
): GameState {
  const { currentShape, cells } = game;

  return collides(currentShape, cells)
    ? flow(
        settleShape,
        tryClearLines,
        spawnNextShape()
      )(game)
    : otherwise(game);
}
