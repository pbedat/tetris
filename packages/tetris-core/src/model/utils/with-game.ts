import { GameState } from "../../types";

function withGame<
  TSliceProp extends keyof GameState,
  TSlice extends GameState[TSliceProp]
>(
  slice: TSliceProp,
  reducer: (slice: TSlice) => TSlice
): (g: GameState) => GameState {
  return (game: GameState) => ({
    ...game,
    [slice]: reducer(game[slice] as any)
  });
}

export default withGame;
