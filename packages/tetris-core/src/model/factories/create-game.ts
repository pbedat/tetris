import { range } from "lodash";

import { createNextShape } from "../reducers/spawn-shape";
import { GameState } from "../../types";

interface GameOptions {
  width: number;
  height: number;
  speed: number;
}

function createGame(options: GameOptions): GameState {
  const { width, height, speed } = options;
  return {
    clearedLines: 0,
    width,
    height,
    cells: range(height).map(() => range(width).map(_ => false)),
    elapsedTime: 0,
    speed,
    score: 0,
    currentShape: createNextShape(),
    nextShape: createNextShape()
  };
}

export default createGame;
