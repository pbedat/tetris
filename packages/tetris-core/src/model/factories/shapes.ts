import { isEmpty, max, range, flatten, flow, negate } from "lodash";
import { filter } from "lodash/fp";
import { Shape, Point } from "../../types";

const T = `
. . .
# # #
. # .
`;

const L = `
# . .
# . .
# # .
`;

const Q = `
# #
# #
`;

const I = `
# # # #
. . . .
`;

const S = `
. . .
. # #
# # .
`;

/**
 * Shapes are defined in a coordinate system
 *    0 1 2
 *   +------>
 * 0 |# # #
 * 1 |  #
 * 2 |
 *   v
 *
 * They are defined in the order of a scan line,
 * starting at (0,0) moving horizontal to the right and then vertically "up"
 */
const shapes = [
  ...[T, L, Q, I, S].map(createShape),
  ...[L, S].map(flow(createShape, mirrorX))
];

export default shapes;

/**
 * Creates a `Shape` from a `string` blueprint
 * Every '#' defines a cell of the shape, other characters are empty coordinates. Whitespaces are ignored.
 * The center of this blueprint becomes the center of the shape.
 * @param blueprint
 */
function createShape(blueprint: string): Shape {
  const stringMatrix = blueprint
    .replace(/ /g, "")
    .split("\n")
    .filter(negate(isEmpty))
    .map(str => Array.from(str));

  const center: Point = [
    (stringMatrix[0].length - 1) / 2,
    (stringMatrix.length - 1) / 2
  ];

  const cells = apply(
    stringMatrix,
    flow(
      flatMapMatrix((cell, point) => (cell === "#" ? point : null)),
      filter(isCell)
    )
  );

  return {
    center,
    cells
  };
}

// TODO move to utils
/**
 * Applies the map `fn` to every cell in a two dimensional array ("matrix").
 * The fn receives also the [x, y] coordinates of the cell
 * @param fn
 */
function flatMapMatrix<TIn, TOut>(
  fn: (cell: TIn, point: Point) => TOut
): (matrix: TIn[][]) => TOut[] {
  return (matrix: TIn[][]) =>
    flatten(matrix.map((row, y) => row.map((cell, x) => fn(cell, [x, y]))));
}

// TODO move to utils
function apply<T, TOut>(coll: T, fn: (items: T) => TOut) {
  return fn(coll);
}

function isCell(cell: null | number[]): cell is Point {
  return cell !== null;
}

function mirrorX(shape: Shape): Shape {
  const { center, cells } = shape;
  const [cX, cY] = center;
  return {
    center,
    cells: cells.map(([x, y]) => [cX - x + cX, y])
  };
}

export function shapeToString(shape: Shape) {
  const width = max(shape.cells.map(max))! + 1;

  return range(0, width)
    .map(y =>
      range(0, width)
        .map(x =>
          shape.cells.find(([pX, pY]) => pX === x && pY === y) ? "#" : "."
        )
        .join(" ")
    )
    .join("\n");
}

setInterval(() => {}, 1000);
