import { flow } from "lodash";
import { GameState, Actions } from "../types";
import getMotion from "./selectors/get-motion";
import willCollide from "./selectors/will-colide";
import settleShape from "./reducers/settle-shape";
import { tryClearLines } from "./reducers/try-clear-lines";
import { applyTime } from "./reducers/apply-time";
import { withoutClipping } from "./middleware/without-clipping";
import withGame from "./utils/with-game";
import rotateShape from "./reducers/rotate-shape";
import translateShape from "./reducers/translate-shape";
import spawnNextShape from "./reducers/spawn-shape";
import createGame from "./factories/create-game";
import { GetAction, InitializeRenderer } from "../io/types";
import endGame from "./reducers/end-game";
import isGameOver from "./selectors/is-game-over";
import score from "./reducers/score";

async function* gameLoop(
  getAction: GetAction,
  initRenderer: InitializeRenderer
) {
  let game = createGame({ width: 10, height: 20, speed: 1000 });

  const framerate = 75;

  const renderGame = initRenderer(game);

  for (;;) {
    renderGame(game);

    yield game;
    await delay(framerate);

    const nextGame = tick(game, getAction(), framerate);

    game = nextGame;
  }
}

export function* syncGameLoop() {
  let game = createGame({ width: 10, height: 20, speed: 1000 });

  const framerate = 75;

  yield game;

  for (;;) {
    const action = yield game;

    const nextGame = tick(game, action || { type: "NOOP" }, framerate);

    game = nextGame;
  }
}

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function tick(game: GameState, action: Actions, delay: number): GameState {
  if (isGameOver(game)) {
    return endGame(game);
  }

  const motion = getMotion(game, action, delay);

  if (willCollide(game, motion)) {
    return flow(
      settleShape,
      score,
      tryClearLines,
      spawnNextShape,
      applyTime(delay)
    )(game);
  }

  return flow(
    withoutClipping(
      withGame(
        "currentShape",
        rotateShape(action.type === "ROTATE" ? action.rotation : 0)
      )
    ),
    withGame("currentShape", translateShape(motion)),
    applyTime(delay)
  )(game);
}

export default gameLoop;
