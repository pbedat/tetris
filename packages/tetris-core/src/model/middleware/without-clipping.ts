import { GameState } from "../../types";
import { ofAny } from "../../utils";

/**
 * Enhances a reducer to not result in shapes that are either
 * outside the game fields boundaries
 * or intersect with already settled blocks
 * @param reducer
 */
export function withoutClipping(reducer: (prevGame: GameState) => GameState) {
  return (game: GameState) => {
    const nextGame = reducer(game);

    const { currentShape } = nextGame;

    const isOutOfBounds = ([x, y]: [number, number]) =>
      x < 0 || y < 0 || y >= nextGame.height || x >= nextGame.width;
    const clipsCells = ([x, y]: [number, number]) => !!nextGame.cells[y][x];

    const hasClipping = currentShape.cells.some(
      ofAny(isOutOfBounds, clipsCells)
    );

    return hasClipping ? game : nextGame;
  };
}
