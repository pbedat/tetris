import { Actions, GameState } from "../types";

export type GetAction = () => Actions;
export type InitializeRenderer = (game: GameState) => RenderGame;
export type RenderGame = (game: GameState) => void;
