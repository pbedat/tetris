import { memoize } from "lodash";

export const value = memoize((val: any) => {
  return () => val;
});

export function ofAny(...fns: Array<(...args: any[]) => boolean>) {
  return (val: any) => fns.some(fn => fn(val));
}
