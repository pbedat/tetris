import { compact, flatten, range } from "lodash";
import * as readline from "readline";

import { GameState, Shape, getLevel } from "pbedat-tetris-core";
import { rowLayout, columnLayout, box } from "./utils";
import { value } from "pbedat-tetris-core/utils";

function createRenderer(game: GameState) {
  readline.cursorTo(process.stdout, 0, 0);
  // clear screen
  range(game.height).forEach(() =>
    process.stdout.write(
      range(100)
        .map(value(" "))
        .join("") + "\n"
    )
  );

  const renderBuffer: string[][] = range(game.height).map(() => []);

  return (game: GameState) => printGame(game, renderBuffer);
}

function printGame(game: GameState, renderBuffer: string[][]) {
  const changes = reconcile(renderBuffer, renderGame(game));

  for (const { x, y, value } of changes) {
    renderBuffer[y][x] = value;
    readline.cursorTo(process.stdout, x, y);
    process.stdout.write(value);
  }

  // move the cursor out of sight
  readline.cursorTo(process.stdout, 1000, 0);
}

function renderGame(game: GameState) {
  const border = ["|", "|"];

  const field = game.cells.map((row, y) => [
    ...border,
    ...row.map((cell, x) => {
      if (
        game.currentShape.cells.find(cell => cell[0] === x && cell[1] === y)
      ) {
        return "#";
      }
      return cell ? "o" : " ";
    }),
    ...border
  ]);

  return rowLayout(
    field,
    box(
      { paddingLeft: 1 },
      columnLayout(
        box({ border: true }, renderScore(game.score)),
        box({ border: true }, renderNextShape(game.nextShape)),
        box({ border: true }, renderLevel(getLevel(game)))
      )
    )
  );
}

function renderLevel(level: number) {
  return [Array.from("LEVEL"), Array.from(level.toString())];
}

function renderScore(score: number) {
  return [
    Array.from("SCORE "),
    [
      ...range(score > 0 ? 5 - Math.log10(score) : 5).map(value("0")),
      ...Array.from(`${score}`)
    ]
  ];
}

function renderNextShape(shape: Shape) {
  return [
    Array.from("NEXT"),
    ...range(4).map(y =>
      range(4).map(x =>
        shape.cells.find(([cX, cY]) => y === cY && x === cX) ? "#" : " "
      )
    )
  ];
}

function reconcile(prevBuffer: string[][], currentBuffer: string[][]) {
  return compact(
    flatten(
      currentBuffer.map((row, y) =>
        row.map((cell, x) => {
          if (!prevBuffer[y] || cell !== prevBuffer[y][x]) {
            return { x, y, value: currentBuffer[y][x]! };
          }
        })
      )
    )
  );
}

export default createRenderer;
