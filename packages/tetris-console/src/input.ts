import { emitKeypressEvents, Key } from "readline";
import { Actions, Rotations } from "pbedat-tetris-core";

let action: Actions = { type: "NOOP" };

export function registerInput() {
  emitKeypressEvents(process.stdin);
  process.stdin.setRawMode!(true);

  process.stdin.on("keypress", handleKeyPress);

  function handleKeyPress(str: string, key: Key) {
    if (key.ctrl && key.name === "c") {
      process.exit(0);
    }

    switch (key.name) {
      case "left":
        action = { type: "MOVE", direction: [-1, 0] };
        break;
      case "right":
        action = { type: "MOVE", direction: [1, 0] };
        break;
      case "down":
        action = { type: "MOVE", direction: [0, 1] };
        break;
      case "a":
        action = { type: "ROTATE", rotation: Rotations.Counterclockwise };
        break;
      case "d":
        action = { type: "ROTATE", rotation: Rotations.Clockwise };
        break;
    }
  }

  return () => process.stdin.off("keypress", handleKeyPress);
}

export function getAction() {
  try {
    return action;
  } finally {
    action = { type: "NOOP" };
  }
}
