import { gameLoop } from "pbedat-tetris-core";
import { registerInput, getAction } from "./input";
import render from "./render";
import { cursorTo } from "readline";

registerInput();

main();

async function main() {
  for await (const game of gameLoop(getAction, render)) {
    if (game.gameOver) {
      cursorTo(process.stdout, 0, game.height + 2);
      process.stdout.write("\n>>>>>>>>> GAME OVER <<<<<<<<<<\n");
      process.stdout.write(`Score: ${game.score}\n\n`);
      break;
    }
  }

  process.exit(0);
}
