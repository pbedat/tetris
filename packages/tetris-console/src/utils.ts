import { drop, max, flatten, range, flow, identity } from "lodash";
import { produce } from "immer";
import { value } from "pbedat-tetris-core";

export function rowLayout(...buffers: string[][][]) {
  const screen = buffers[0];
  return produce(screen, s => {
    for (const buffer of drop(buffers, 1)) {
      for (let y = 0; y < Math.max(s.length, buffer.length); y++) {
        s[y] = (s[y] || []).concat(buffer[y] || []);
      }
    }
  });
}

export function columnLayout(...buffers: string[][][]) {
  return flatten(buffers);
}

export function box(
  styles: { border?: boolean; paddingLeft?: number },
  buffer: string[][]
) {
  return flow(
    fillEmpty,
    styles.border ? border : identity,
    padLeft(styles.paddingLeft)
  )(buffer);
}

function padLeft(space?: number) {
  return (buffer: string[][]) =>
    space
      ? buffer!.map(row => [...range(space).map(value(" ")), ...row])
      : buffer;
}

function border(buffer: string[][]): string[][] {
  return buffer.length
    ? [
        ["+", ...buffer[0].map(value("-")), "+"],
        ...buffer.map(row => ["|", ...row, "|"]),
        ["+", ...buffer[0].map(value("-")), "+"]
      ]
    : [["+"]];
}

function fillEmpty(buffer: string[][]): string[][] {
  const maxWidth = max(buffer.map(b => b.length)) || 0;

  return buffer.map(row => [
    ...row,
    ...range(maxWidth - row.length).map(value(" "))
  ]);
}
